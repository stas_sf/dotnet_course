using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Text57");
            StreamReader sr = new StreamReader(GetString(), Encoding.Default);
            BinaryWriter bw = new BinaryWriter(new FileStream(GetString(), FileMode.Create), Encoding.Default);

            SortedDictionary<char, int> d = new SortedDictionary<char, int>();

            string s = sr.ReadToEnd();
            for (char c = '�'; c <= '�'; ++c)
            {
                d[c] = 0;
            }
            d['�'] = 0;

            for (int i = 0; i < s.Length; ++i)
            {
                if (char.IsLower(s[i]))
                {
                    ++d[s[i]];
                }
            }

            foreach(KeyValuePair<char,int> p in d)
            {
                if (p.Value != 0)
                {
                    bw.Write(string.Format("{0}-{1}", p.Key, p.Value).PadRight(80));
                }
            }

            sr.Close();
            bw.Close();
        }
    }
}




