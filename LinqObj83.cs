using PT4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        // ��� ������ ������ ����� �� ��������� ���������� �����
        // � ������ a ���� string[] ����������� ��������:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // ��� ������ ������������������ s ���� IEnumerable<string>
        // � �������������� ��������� ���� ����������� ��������:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // ��� ������� ����� ������ LinqObj �������� ���������
        // �������������� ������ ����������, ������������ � ���������:
        //
        //   Show() � Show(cmt) - ���������� ������ ������������������,
        //     cmt - ��������� �����������;
        //
        //   Show(e => r) � Show(cmt, e => r) - ���������� ������
        //     �������� r, ���������� �� ��������� e ������������������,
        //     cmt - ��������� �����������.

        public static void Solve()
        {
            Task("LinqObj83");
            var b = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    country = ss[0],
                    article = ss[1],
                    category = ss[2]
                };
            });
            var d = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    price = int.Parse(ss[0]),
                    namen = ss[1],
                    article = ss[2]
                };
            });
            var e = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    namen = ss[0],
                    article = ss[1],
                    consumerId = int.Parse(ss[2])
                };
            });
            var jn1 = b.Join(d, bb => bb.article, dd => dd.article, (bb, dd) =>
                new
                {
                    article = bb.article,
                    country = bb.country,
                    category = bb.category,
                    price = dd.price,
                    namen = dd.namen
                });
            var jn2 = jn1.Join(e, j => j.namen, ee => ee.namen, (j, ee) =>
                new
                {
                    article = j.article,
                    country = j.country,
                    category = j.category,
                    price = j.price,
                    namen = j.namen,
                    consumerId = ee.consumerId
                });

            var grouped = jn2.GroupBy(a => a.namen, (k,nn) =>
                nn.GroupBy(x => x.country, (kk, xx) =>
                    new {country = kk, namen = k, totalSum = xx.Sum(g => g.price) }));
            foreach (var k in grouped)
            {
                k.Show();
            }

        }
    }
}
