using PT4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        // ��� ������ ������ ����� �� ��������� ���������� �����
        // � ������ a ���� string[] ����������� ��������:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // ��� ������ ������������������ s ���� IEnumerable<string>
        // � �������������� ��������� ���� ����������� ��������:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // ��� ������� ����� ������ LinqObj �������� ���������
        // �������������� ������ ����������, ������������ � ���������:
        //
        //   Show() � Show(cmt) - ���������� ������ ������������������,
        //     cmt - ��������� �����������;
        //
        //   Show(e => r) � Show(cmt, e => r) - ���������� ������
        //     �������� r, ���������� �� ��������� e ������������������,
        //     cmt - ��������� �����������.

        public static void Solve()
        {
            Task("LinqObj71");
            var a = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    id = int.Parse(ss[0]),
                    year = int.Parse(ss[1]),
                    street = ss[2]
                };
            });
            var c = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    id = int.Parse(ss[0]),
                    discount = int.Parse(ss[1]),
                    namen = ss[2]
                };
            });
            var markets = from e1 in a
                          join e2 in c
                          on e1.id equals e2.id
                          select new { namen = e2.namen, cons = e1.id, year = e1.year, discount = e2.discount };
            var grouped = from e in markets
                          group e by e.namen;
            var marketsWithMaxDiscount = from e in grouped
                                         let md = e.Max(x => x.discount)
                                         from x in e
                                         where x.discount == md
                                         orderby x.namen, x.cons
                                         select x;
            var res = from e in marketsWithMaxDiscount
                      group e by e.namen;
            var nn = from e in res
                     let r = e.First()
                     select string.Format("{0} {1} {2} {3}", r.namen, r.cons, r.year, r.discount);
            File.WriteAllLines(GetString(), nn.ToArray(), Encoding.Default);
        }
    }
}
