using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Text40");
            BinaryReader br1 = new BinaryReader(new FileStream(GetString(), FileMode.Open));
            BinaryReader br2 = new BinaryReader(new FileStream(GetString(), FileMode.Open));
            StreamWriter sw = new StreamWriter(GetString(), false, Encoding.Default);

            while(br1.BaseStream.Position < br1.BaseStream.Length)
            {

                int a = br1.ReadInt32();
                int b = br2.ReadInt32();
                sw.WriteLine("|{0,30}{1,30}|", a, b);
            }

            br1.Close();
            br2.Close();
            sw.Close();
        }
    }
}




