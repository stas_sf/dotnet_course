using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Array44");
            int n = GetInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; ++i)
                arr[i] = GetInt();

            int i1, i2;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < i; ++j)
                    if (arr[i] == arr[j])
                    {
                        i1 = j + 1;
                        i2 = i + 1;
                        Put(i1, i2);
                        return;
                    }
        }
    }
}




