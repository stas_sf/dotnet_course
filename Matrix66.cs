using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Matrix66");
            int m = GetInt();
            int n = GetInt();
            List<List<double>> matr = new List<List<double>>();

            for (int i = 0; i < m; ++i)
            {
                matr.Add(new List<double>());
                for (int j = 0; j < n; ++j)
                    matr[i].Add(GetDouble());
            }

            // ����� ���������� ������� � �������������� ����������
            int ind = -1;
            for (int j = 0; j < n; ++j)
            {
                bool minus = true;
                for (int i = 0; i < m; ++i)
                {
                    if (matr[i][j] >= 0)
                    {
                        minus = false;
                        break;
                    }
                }
                if (minus)
                    ind = j;
            }

            //�������� �������
            if (ind != -1)
            {
                for (int i = 0; i < m; ++i)
                {
                    matr[i].RemoveAt(ind);
                }
            }
            
            for (int i = 0; i < matr.Count; ++i)
                for (int j = 0; j < matr[i].Count; ++j)
                    Put(matr[i][j]);
        }
    }
}




