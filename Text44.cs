using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Text44");
            StreamReader sr = new StreamReader(GetString(), Encoding.Default);
            char[] separator = {' '};

            int sum = 0;
            int count = 0;
            while(!sr.EndOfStream)
            {
                sum += int.Parse(sr.ReadLine());
                ++count;
            }
            sr.Close();

            Put(count, sum);;
        }
    }
}




