using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("File11");
            FileStream fs = new FileStream(GetString(), FileMode.Open);
            BinaryWriter bwOdd = new BinaryWriter(File.Open(GetString(), FileMode.CreateNew));
            BinaryWriter bwEven = new BinaryWriter(File.Open(GetString(), FileMode.CreateNew));
            BinaryReader br = new BinaryReader(fs);
            while (fs.Position < fs.Length)
            {
                double k = br.ReadDouble();
                bwOdd.Write(k);
                if (fs.Position >= fs.Length)
                    break;
                k = br.ReadDouble();
                bwEven.Write(k);
            }
            br.Close();
            bwOdd.Close();
            bwEven.Close();
        }
    }
}




