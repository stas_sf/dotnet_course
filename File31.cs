using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("File31");
            string fname = GetString();
            string tempFname = "temp_$1";
            int count = 50;

            FileStream fs = new FileStream(fname, FileMode.Open);
            FileStream fsResult = new FileStream(tempFname, FileMode.CreateNew);
            BinaryReader br = new BinaryReader(fs);
            BinaryWriter bw = new BinaryWriter(fsResult);

            fsResult.SetLength(sizeof(int) * count);
            fs.Seek(fs.Length - count * sizeof(int), SeekOrigin.Begin);

            for (int i = 0; i < count; ++i)
            {
                bw.Write(br.ReadInt32());
            }

            fs.Close();
            fsResult.Close();
            File.Delete(fname);
            File.Move(tempFname, fname);
        }
    }
}




