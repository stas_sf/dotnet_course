using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Matrix39");
            int m = GetInt();
            int n = GetInt();
            int[,] matr = new int[m, n];

            for (int i = 0; i < m; ++i)
                for (int j = 0; j < n; ++j)
                    matr[i, j] = GetInt();

            int count = n;
            for (int j = 0; j < n; ++j)
            {
                bool cancel = false;
                for (int i = 0; i < m; ++i)
                {
                    if (cancel)
                        break;
                    for (int k = i + 1; k < m; ++k)
                        if (matr[i, j] == matr[k, j])
                        {
                            --count;
                            cancel = true;
                            break;
                        }
                }
            }

            Put(count);
        }
    }
}




