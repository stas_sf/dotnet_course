using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Matrix32");
            int m = GetInt();
            int n = GetInt();
            int[,] matr = new int[m,n];

            for (int i = 0; i < m; ++i)
                for (int j = 0; j < n; ++j)
                    matr[i, j] = GetInt();
            
            for (int i = 0; i < m; ++i)
            {
                int count = 0;
                for (int j = 0; j < n; ++j)
                {
                    if (matr[i, j] > 0)
                        ++count;
                    else if (matr[i, j] < 0)
                        --count;
                }
                if (count == 0)
                {
                    Put(i+1);
                    return;
                }
            }

            Put(0);
        }
    }
}