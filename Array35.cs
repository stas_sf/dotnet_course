using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Array35");

            int n = GetInt(); //size

            double[] arr = new double[n+2];
            arr[0] = arr[arr.Length-1] = double.MinValue;
            for (int i = 1; i < arr.Length-1; ++i)
                arr[i] = GetDouble();

            double minLocalMax = double.MaxValue;
            for (int i = 1; i < arr.Length - 1; ++i)
                if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1] && arr[i] < minLocalMax)
                    minLocalMax = arr[i];

            Put(minLocalMax);
        }
    }
}




