using PT4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        // ��� ������ ������ ����� �� ��������� ���������� �����
        // � ������ a ���� string[] ����������� ��������:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // ��� ������ ������������������ s ���� IEnumerable<string>
        // � �������������� ��������� ���� ����������� ��������:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // ��� ������� ����� ������ LinqObj �������� ���������
        // �������������� ������ ����������, ������������ � ���������:
        //
        //   Show() � Show(cmt) - ���������� ������ ������������������,
        //     cmt - ��������� �����������;
        //
        //   Show(e => r) � Show(cmt, e => r) - ���������� ������
        //     �������� r, ���������� �� ��������� e ������������������,
        //     cmt - ��������� �����������.

        public static void Solve()
        {
            Task("LinqObj64");
            var marks = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    group = int.Parse(ss[0]),
                    name = ss[1] + " " + ss[2],
                    course = ss[3],
                    mark = byte.Parse(ss[4])
                };
            }).Where(e => e.course == "�����������")
              .GroupBy(e => e.name, (k, nn) =>
                  new { name = k, group = nn.First().group, mark = nn.Average(x => x.mark)})
              .Where(e => e.mark >= 4)
              .OrderBy(e => e.group).ThenBy(e => e.name)
              .Select(e => string.Format(CultureInfo.InvariantCulture, "{0} {1} {2:F2}", e.group, e.name, e.mark))
              .DefaultIfEmpty("��������� �������� �� �������");
            File.WriteAllLines(GetString(), marks.ToArray(), Encoding.Default);
        }
    }
}
