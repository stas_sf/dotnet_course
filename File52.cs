using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("File52");
            string s0 = GetString();
            int n = GetInt();
            string[] s = new string[n];
            for (int i = 0; i < n; ++i)
            {
                s[i] = GetString();
            }

            FileStream[] fstreams = new FileStream[n];
            BinaryReader[] brs = new BinaryReader[n];
            for (int i = 0; i < n; ++i)
            {
                fstreams[i] = new FileStream(s[i], FileMode.Open);
                brs[i] = new BinaryReader(fstreams[i]);
            }
            FileStream fsRes = new FileStream(s0, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fsRes);

            bw.Write(n);
            for (int i = 0; i < n; ++i)
            {
                bw.Write((int)fstreams[i].Length / sizeof(int));
            }

            for (int i = 0; i < n; ++i)
            {
                while (fstreams[i].Position < fstreams[i].Length)
                {
                    bw.Write(brs[i].ReadInt32());
                }
            }

            for (int i = 0; i < n; ++i)
            {
                fstreams[i].Close();
            }
            fsRes.Close();
        }
    }
}




