using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PT4;

namespace PT4Tasks
{
    public class MyTask : PT
    {

        // Для чтения набора строк из исходного текстового файла 
        // в массив a типа string[] используйте оператор:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // Для записи последовательности s типа IEnumerable<string>
        // в результирующий текстовый файл используйте оператор:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // При решении задач группы LinqObj доступны следующие
        // дополнительные методы расширения, определенные в задачнике:
        //
        //   Show() и Show(cmt) - отладочная печать последовательности, 
        //     cmt - строковый комментарий;
        //
        //   Show(e => r) и Show(cmt, e => r) - отладочная печать
        //     значений r, полученных из элементов e последовательности, 
        //     cmt - строковый комментарий.

        public static void Solve()
        {
            Task("LinqObj49");
            var schoolkids = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                return new
                {
                    name = ss[0] + " " + ss[1],
                    school = ss[2],
                    rus = int.Parse(ss[3]),
                    inf = int.Parse(ss[4]),
                    math = int.Parse(ss[5]),
                    sum = int.Parse(ss[3]) + int.Parse(ss[4]) + int.Parse(ss[5])
                };
            });
            int min = schoolkids.Min(e => e.sum);
            var res = schoolkids.Where(e => e.sum == min).Select(e => e.name + " " + e.school);
            string[] t = { min.ToString() };
            var n = t.Concat(res);
            File.WriteAllLines(GetString(), n.ToArray(), Encoding.Default);
        }
    }

    // --------------------------------------------------------------
    // Реализация методов расширения Show
    // --------------------------------------------------------------
    static class LinqObj
    {
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            string cmt, Func<TSource, TResult> selector)
        {
            var b = a.Select(selector).ToArray();
            PT.Show(cmt);
            PT.Show((b.Length + ":").PadLeft(3));
            foreach (var e in b)
                PT.Show(e);
            PT.ShowLine();
            return a;
        }
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            Func<TSource, TResult> selector)
        {
            return a.Show("", selector);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a, string cmt)
        {
            return a.Show(cmt, e => e);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a)
        {
            return a.Show("", e => e);
        }
    }

}


