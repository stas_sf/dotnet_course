using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PT4;

namespace PT4Tasks
{
    public class MyTask : PT
    {

        // Для чтения набора строк из исходного текстового файла 
        // в массив a типа string[] используйте оператор:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // Для записи последовательности s типа IEnumerable<string>
        // в результирующий текстовый файл используйте оператор:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // При решении задач группы LinqObj доступны следующие
        // дополнительные методы расширения, определенные в задачнике:
        //
        //   Show() и Show(cmt) - отладочная печать последовательности, 
        //     cmt - строковый комментарий;
        //
        //   Show(e => r) и Show(cmt, e => r) - отладочная печать
        //     значений r, полученных из элементов e последовательности, 
        //     cmt - строковый комментарий.

        public static void Solve()
        {
            Task("LinqObj1");
            string[] clients = File.ReadAllLines(GetString(), Encoding.Default);
            var c = clients.Select(s =>
            {
                string[] ss = s.Split();
                return new { id = ss[0], year = ss[1], month = ss[2], hours = int.Parse(ss[3]) };
            }).OrderByDescending(s => s.hours).Last();
            File.WriteAllText(GetString(), c.hours + " " + c.year + " " + c.month, Encoding.Default);
        }
    }

    // --------------------------------------------------------------
    // Реализация методов расширения Show
    // --------------------------------------------------------------
    static class LinqObj
    {
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            string cmt, Func<TSource, TResult> selector)
        {
            var b = a.Select(selector).ToArray();
            PT.Show(cmt);
            PT.Show((b.Length + ":").PadLeft(3));
            foreach (var e in b)
                PT.Show(e);
            PT.ShowLine();
            return a;
        }
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            Func<TSource, TResult> selector)
        {
            return a.Show("", selector);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a, string cmt)
        {
            return a.Show(cmt, e => e);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a)
        {
            return a.Show("", e => e);
        }
    }

}


