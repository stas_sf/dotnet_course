using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PT4;
using System.Globalization;

namespace PT4Tasks
{
    public class MyTask : PT
    {

        // Для чтения набора строк из исходного текстового файла 
        // в массив a типа string[] используйте оператор:
        //
        //   a = File.ReadAllLines(GetString(), Encoding.Default);
        //
        // Для записи последовательности s типа IEnumerable<string>
        // в результирующий текстовый файл используйте оператор:
        //
        //   File.WriteAllLines(GetString(), s.ToArray(), Encoding.Default);
        //
        // При решении задач группы LinqObj доступны следующие
        // дополнительные методы расширения, определенные в задачнике:
        //
        //   Show() и Show(cmt) - отладочная печать последовательности, 
        //     cmt - строковый комментарий;
        //
        //   Show(e => r) и Show(cmt, e => r) - отладочная печать
        //     значений r, полученных из элементов e последовательности, 
        //     cmt - строковый комментарий.

        static int BlockNumber(int flatNum)
        {
            return (flatNum - 1) / 36 + 1;
        }

        public static void Solve()
        {
            Task("LinqObj29");
            var n = File.ReadAllLines(GetString(), Encoding.Default).Select(s =>
            {
                string[] ss = s.Split();
                string[] cred = ss[2].Split('.');
                return new { block = BlockNumber(int.Parse(ss[0])), flat = ss[0], surname = ss[1],
                    credit = double.Parse(ss[2],CultureInfo.InvariantCulture),
                    credit_str = string.Join(".",cred)
                };
            }).GroupBy(e => e.block, (k, p) => p.OrderByDescending(a => a.credit).First())
              .OrderBy(e => e.block).Select(e => {
                  return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2} {3:F2}",
                      e.block, e.flat, e.surname, e.credit);
              });
            File.WriteAllLines(GetString(), n.ToArray(), Encoding.Default);
        }
    }

    // --------------------------------------------------------------
    // Реализация методов расширения Show
    // --------------------------------------------------------------
    static class LinqObj
    {
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            string cmt, Func<TSource, TResult> selector)
        {
            var b = a.Select(selector).ToArray();
            PT.Show(cmt);
            PT.Show((b.Length + ":").PadLeft(3));
            foreach (var e in b)
                PT.Show(e);
            PT.ShowLine();
            return a;
        }
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            Func<TSource, TResult> selector)
        {
            return a.Show("", selector);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a, string cmt)
        {
            return a.Show(cmt, e => e);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a)
        {
            return a.Show("", e => e);
        }
    }

}


