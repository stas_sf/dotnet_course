using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask : PT
    {

        // При решении задач группы LinqBegin доступны следующие
        // дополнительные методы, определенные в задачнике:
        //
        //   GetEnumerableInt() - ввод числовой последовательности;
        //
        //   GetEnumerableString() - ввод строковой последовательности;
        //
        //   Put() (метод расширения) - вывод последовательности;
        //
        //   Show() и Show(cmt) (методы расширения) - отладочная печать 
        //     последовательности, cmt - строковый комментарий;
        //
        //   Show(e => r) и Show(cmt, e => r) (методы расширения) - 
        //     отладочная печать значений r, полученных из элементов e
        //     последовательности, cmt - строковый комментарий.

        public static void Solve()
        {
            Task("LinqBegin8");
            var res = GetEnumerableInt().Where(e => (e / 100 == 0 && e / 10 > 0));
            Put(res.Count(), res.DefaultIfEmpty(0).Average());
            
        }

        // --------------------------------------------------------------
        // Реализация методов GetEnumerableInt() и GetEnumerableString()
        // --------------------------------------------------------------
        static IEnumerable<int> GetEnumerableInt()
        {
            return Enumerable.Repeat(0, GetInt()).Select(e => GetInt()).ToArray();
        }
        static IEnumerable<string> GetEnumerableString()
        {
            return Enumerable.Repeat("", GetInt()).Select(e => GetString()).ToArray();
        }
    }

    // --------------------------------------------------------------
    // Реализация методов расширения Put и Show
    // --------------------------------------------------------------
    static class LinqBegin
    {
        public static void Put<T>(this IEnumerable<T> a)
        {
            var b = a.ToArray();
            PT.Put(b.Length);
            foreach (T e in b)
                PT.Put(e);
        }
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            string cmt, Func<TSource, TResult> selector)
        {
            var b = a.Select(selector).ToArray();
            PT.Show(cmt);
            PT.Show((b.Length + ":").PadLeft(3));
            foreach (var e in b)
                PT.Show(e);
            PT.ShowLine();
            return a;
        }
        public static IEnumerable<TSource> Show<TSource, TResult>(this IEnumerable<TSource> a,
            Func<TSource, TResult> selector)
        {
            return a.Show("", selector);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a, string cmt)
        {
            return a.Show(cmt, e => e);
        }
        public static IEnumerable<T> Show<T>(this IEnumerable<T> a)
        {
            return a.Show("", e => e);
        }
    }
}


