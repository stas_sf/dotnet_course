using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("Array97");
            int n = GetInt();
            List<int> l = new List<int>();
            for (int i = 0; i < n; ++i)
                l.Add(GetInt());

            for (int i = n - 1; i > 0; --i)
            {
                int pos = l.LastIndexOf(l[i], i-1);
                if (pos != -1)
                    l.RemoveAt(pos);
            }
            
            for (int i = 0; i < l.Count; ++i)
                Put(l[i]);

        }
    }
}




