using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PT4;
using System.IO;

namespace PT4Tasks
{
    public class MyTask: PT
    {
        public static void Solve()
        {
            Task("File67");
            BinaryReader br = new BinaryReader(new FileStream(GetString(), FileMode.Open));
            BinaryWriter bwDays = new BinaryWriter(new FileStream(GetString(), FileMode.Create));
            BinaryWriter bwMonths = new BinaryWriter(new FileStream(GetString(), FileMode.Create));

            while(br.BaseStream.Position < br.BaseStream.Length)
            {
                string[] parts = br.ReadString().Split('/');
                bwDays.Write(int.Parse(parts[0]));
                bwMonths.Write(int.Parse(parts[1]));
            }

            br.Close();
            bwDays.Close();
            bwMonths.Close();
        }
    }
}




